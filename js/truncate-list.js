(function ($) {
  Drupal.behaviors.truncateList = {
    attach: function (context, settings) {
      for (var i=0; i < Drupal.settings.truncate_list.length; i++) {
        $(Drupal.settings.truncate_list[i].selector).truncateList({
          elements: Drupal.settings.truncate_list[i].elements,
          numberOfItems: Drupal.settings.truncate_list[i].numberOfItems,
          moreText: Drupal.settings.truncate_list[i].moreText,
          lessText: Drupal.settings.truncate_list[i].lessText
        });
      }
    }
  }
}(jQuery));
