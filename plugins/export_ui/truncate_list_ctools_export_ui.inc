<?php
/**
 * @file
 * Export UI plugin for truncateList.
 *
 */

/**
 * Defines this Export UI plugin.
 */
$plugin = array(
  'schema' => 'truncate_list_preset',
  'access' => 'administer content',
  'menu' => array(
    'menu item' => 'truncate_list',
    'menu title' => 'TruncateList settings',
    'menu description' => 'Administer truncateList presets.',
  ),
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('truncateList preset'),
  'title plural proper' => t('truncateList presets'),
  'form' => array(
    'settings' => 'truncate_list_ctools_export_ui_form',
  ),
);

/**
 * Defines the preset add/edit form.
 */
function truncate_list_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('The human readable name or description of this preset.'),
    '#default_value' => $preset->description,
    '#required' => true,
  );
  $form['selector'] = array(
    '#type' => 'textfield',
    '#title' => t('selector'),
    '#description' => t('CSS selector for the parent which needs its elements truncated.'),
    '#default_value' => $preset->selector,
    '#required' => true,
  );
  $form['elements'] = array(
    '#type' => 'textfield',
    '#title' => t('Elements'),
    '#description' => t('CSS selector for the elements.'),
    '#default_value' => $preset->elements,
    '#required' => true,
  );
  $form['number_of_items'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items'),
    '#description' => t('Number of elements shown after truncation.'),
    '#default_value' => $preset->number_of_items,
    '#required' => true,
  );
  $form['moretext'] = array(
    '#type' => 'textfield',
    '#title' => t('More text'),
    '#description' => t('More text.'),
    '#default_value' => $preset->moretext,
    '#required' => true,
  );
  $form['lesstext'] = array(
    '#type' => 'textfield',
    '#title' => t('Less text'),
    '#description' => t('Less text.'),
    '#default_value' => $preset->lesstext,
    '#required' => true,
  );
}
